/*Angular slklada sie z dwoch plikow: widok (html) oraz kontroler zawierajacy logike
i zyjacy w nim model
$scope to swego rodzaju obiekt,
 w którym przechowane są dane tworzone przez kontroler.
*/

/*Second example*/

/*angular.module('app', []).controller('FirstCtrl',['$scope', function($scope) {*/
	/*$scope.message = {
		name: "Damian",
		surname : 'Wazny',
		age : 28
	};*/


/*} ]);*/
//angular.module( 'aplikacja' , [] );

/*var app = angular
			.module('app', [])
			.controller("MyController", function($scope) {

			});*/

var app = angular.module('app', []);

app.PierwszyKontroler = function ($scope) {
	$scope.name = 'World';
};

app.DrugiKontroler = function ($scope) {
	$scope.surname = 'Wazny';
};

app.TrzeciKontroler = function ($scope) {

	$scope.function1 = function() {
		$scope.name = 'World....................';
	};
	$scope.function2 = function() {
		$scope.name = 'People how are You';
	};

	$scope.name = 'pelikan krasy';
};

/*----------Wlasny filter----------*/

/*app.filter('split', function() {
	return function (text) {
		return text.split('').join('_');
	};
});*/

/*---------Kontroler z miastami-------------*/
app.SearchCtrl = function ($scope) {
	$scope.cities = [
		'Gdańsk',
		'Gdynia',
		'Sopot',
		'Szczecin',
		'Poznań',
		'Wrocław',
		'Kraków',
		'Łódź',
		'Bydgoszcz',
		'Toruń',
		'Kielce',
		'Rzeszow',
		'Zakopane',
		'Bratysława',
		'Lublin',
		'Tarnow'
	];
};
/*---------Kontroler ze zdjeciami-------------*/
app.ImgCtrl = function ($scope) {
	var img = {
		src: 'assets/images/flaga.jpg',
		alt: 'Flaga USA',
		title: 'Flaga USA'
	};
	$scope.img = img;
};
/*---------Kontroler z danymi osob.-------------*/
app.PersonCtrl = function ($scope) {
	var persons = [
		{ firstName: "Tomek", lastName: "Wazny", gender: "Male", salary: 5500},
		{ firstName: "Ewa", lastName: "Miksa", gender: "Female", salary: 2300},
		{ firstName: "Ola", lastName: "Taciuch", gender: "Female", salary: 6000},
		{ firstName: "Damian", lastName: "Wazny", gender: "Male", salary: 8900}
	];
	$scope.persons = persons;
}
/*---------Kontroler z miastami----------------*/
app.CountriesCtrl = function ($scope) {
	var countries = [
		{ 
			name: 'UK',
			towns: [
				{
					name: "London",
					name: "Doncaster",
					name: "Cheelse",
				}
			]
		}
	];
	$scope.countries = countries;
}
/*---------Kontroler z technologiami----------------*/
app.TechnologiesCtrl = function ($scope) {
	var technologies = [
		{ name: "C#", likes: 0, dislikes: 0 },
		{ name: "ASP.NET", likes: 0, dislikes: 0},
		{ name: "Angular", likes: 0, dislikes: 0},
		{ name: "C++", likes: 0, dislikes: 0}
	];
	$scope.technologies = technologies;

	$scope.incrementLikes = function(technology) {
		technology.likes++;
	}
	$scope.incrementDislikes = function(technology) {
		technology.dislikes++;
	}
}
/*-------------Zwiekszanie i zmniejszanie wartosci-----*/

app.ZwiekszaczCtrl = function($scope) {

	$scope.wartosc = 1;

	$scope.incFunction = function($count) {
		$scope.wartosc= $scope.wartosc+$count;
	}
	$scope.descFunction = function($count) {
		$scope.wartosc= $scope.wartosc-$count;
	}
}

/*app.directive('aaa', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
                elem.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
});*/

/*-------------Petlna od 0 do 100-------------*/
/*app.LoopCtrl = function($scope) {
	$scope.values = [
	"1",
	"2",
	"3",
	"4"
	];
}*/

app.LoopCtrl = function($scope) {

	$scope.CountFunction = function() {
		var values = [];

		for(var i=0; i<=100; i++) {
			values.push(i);
		}	
		/*for(var i=0; i<=100; i++) {
			values.pop(i);
		}*/
		$scope.values = values;
		}
	
}







